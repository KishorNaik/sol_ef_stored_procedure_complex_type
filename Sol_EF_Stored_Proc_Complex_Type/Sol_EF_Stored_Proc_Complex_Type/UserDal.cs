﻿using Sol_EF_Stored_Proc_Complex_Type.EF;
using Sol_EF_Stored_Proc_Complex_Type.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Stored_Proc_Complex_Type
{
    public class UserDal
    {
        #region Declaration
        private UserDBEntities db = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            db = new UserDBEntities();
        }
        #endregion

        #region Public Methods
        public async Task<IEnumerable<UserEntity>> GetUserData(UserEntity userEntityObj)
        {
            ObjectParameter status = null;
            ObjectParameter message = null;

            return await Task.Run(() => {

                var getQuery =
                    db
                    ?.uspGetUser("Select",
                    userEntityObj?.UserId ?? 0,
                    userEntityObj?.FirstName ?? null,
                    userEntityObj?.LastName ?? null,
                    userEntityObj?.UserLogin?.UserName ?? null,
                    userEntityObj?.UserLogin?.Password ?? null,
                    userEntityObj?.UserCommunication?.MobileNo,
                    userEntityObj?.UserCommunication?.EmailId,
                    status = new ObjectParameter("Status", typeof(int)),
                    message = new ObjectParameter("Message", typeof(string))
                    )
                    ?.AsEnumerable()
                    ?.Select(this.SelectUser)
                    ?.ToList();

                return getQuery;
            });
        }
        #endregion

        #region Private Property
        private Func<uspGetUserResultSet,UserEntity> SelectUser
        {
            get
            {
                return
                      (leUspGetUserResultSet) => new UserEntity()
                      {
                          FirstName = leUspGetUserResultSet.FirstName,
                          LastName = leUspGetUserResultSet.LastName,
                          UserId = leUspGetUserResultSet.UserId,
                          UserCommunication = new UserCommunication()
                          {
                              EmailId = leUspGetUserResultSet.EmailId,
                              MobileNo = leUspGetUserResultSet.MobileNo
                          },
                          UserLogin = new UserLoginEntity()
                          {
                              UserName = leUspGetUserResultSet.UserName,
                              Password = leUspGetUserResultSet.Password
                          }
                      };
            }
        }
        #endregion 
    }
}
