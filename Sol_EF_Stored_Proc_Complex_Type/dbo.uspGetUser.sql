﻿CREATE PROCEDURE [dbo].[uspGetUser]
	
	@Command Varchar(50)=NULL,
	
	@UserId Numeric(18,0)=NULL,

	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,

	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@MobileNo Varchar(50)=NULL,
	@EmailId Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
AS
	
	DECLARE @ErrorMessage Varchar(MAX)

	IF @Command='Select'
		BEGIN
			
			BEGIN TRANSACTION

				BEGIN TRY 
						
						SELECT 
							U.UserId,
							U.FirstName,
							U.LastName
							FROM tblUsers AS U 

						SET @Status=1
						SET @Message='Get Select Data'

					COMMIT TRANSACTION

				END TRY 
				
				BEGIN CATCH 
					
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Select Exception'

						RAISERROR(@ErrorMessage,16,1)

				END CATCH 

		END 
	ELSE IF @Command='Search'
		BEGIN
			
			BEGIN TRANSACTION

				BEGIN TRY 
						
						-- Sub Query
						--SELECT UC.UserId
						--	FROM tblUsersCommunication AS UC
						--		WHERE UC.MobileNo=@MobileNo

						--SELECT 
						--	UL.UserName,
						--	Ul.Password
						--	FROM tblUserLogin AS UL
						--		WHERE UL.UserId=1

						--SELECT 
						--	UL.UserName,
						--	UL.Password
						--		FROM tblUserLogin AS UL
						--			WHERE UL.UserId IN 
						--				(
						--					SELECT UC.UserId
						--						FROM tblUsersCommunication AS UC 
						--							WHERE UC.MobileNo=@MobileNo
						--				)

						-- Using Join
						SELECT 
							U.UserId,
							U.FirstName,
							U.LastName
							FROM tblUsers AS U
								WHERE FirstName=@FirstName
								

						SET @Status=1
						SET @Message='Get Search Data'

					COMMIT TRANSACTION

				END TRY 
				
				BEGIN CATCH 
					
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Search Mobile No Exception'

						RAISERROR(@ErrorMessage,16,1)

				END CATCH 

		END 

	ELSE IF @Command='GetById'
		BEGIN
			
			BEGIN TRANSACTION

				BEGIN TRY 
						
						SELECT 
							U.UserId,
							U.FirstName,
							U.LastName
							FROM tblUsers AS U 
								WHERE U.UserId=@UserId

						SET @Status=1
						SET @Message='Get Search Data'

					COMMIT TRANSACTION

				END TRY 
				
				BEGIN CATCH 
					
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Search Mobile No Exception'

						RAISERROR(@ErrorMessage,16,1)

				END CATCH 

		END